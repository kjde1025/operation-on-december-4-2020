<%--
  Created by IntelliJ IDEA.
  User: daier
  Date: 2020/12/4
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %> <%--不要忽略EL表达式--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <div align="center" style="position: relative;top: 150px">
        <table style="border: silver solid 1px">
            <tr>
                <td>性别：</td>
                <td>
                    <c:if test="${sex==1}">男</c:if>
                    <c:if test="${sex==0}">女</c:if>
                </td>
            </tr>
            <tr>
                <td>年龄：</td>
                <td>${age}</td>
            </tr>
            <tr>
                <td>高压：</td>
                <td>${high}</td>
            </tr>
            <tr>
                <td>低压：</td>
                <td>${low}</td>
            </tr>
            <tr>
                <td>健康状况：</td>
                <td>${condition}</td>
            </tr>
        </table>
    </div>
</body>
</html>
