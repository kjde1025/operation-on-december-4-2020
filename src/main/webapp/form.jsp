<%--
  Created by IntelliJ IDEA.
  User: daier
  Date: 2020/12/4
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %> <%--不要忽略EL表达式--%>
<html>
<head>
    <meta charset="UTF-8">
    <title>表单</title>
</head>
<body>
<div align="center" style="margin-top: 5%">
<form action="/out" method="post">
    <table style="border: 1px solid silver;">
        <tr>
            <td>性别:</td>
            <td>
                <input type="radio" value="1" name="sex" checked/>男&nbsp;
                <input type="radio" value="0" name="sex"/>女
            </td>
        </tr>
        <tr>
            <td>年龄:</td>
            <td>
                <input type="text" value="" name="age" style="width:75px; height:20px" placeholder="请输入年龄"/>
            </td>
        </tr>
        <tr>
            <td>高低压:</td>
            <td>
                <input type="text" value="" name="high" style="width:75px; height:20px" placeholder="请输入高压"/>,
                <input type="text" value="" name="low"style="width:75px; height:20px"  placeholder="请输入低压"/>
            </td>
        </tr>
    </table>
    <input type="submit" value="提交" style="position: relative;left: 91px"/>
</form>
</div>
</body>
</html>
