package servlet;

import entity.Person;
import service.Test;
import service.TestImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author：时尚
 * @日期： 12 2020/12/4 14:31
 */
public class FormServlet extends HttpServlet {
    private Test test = new TestImpl();
    private static final long serialVersionUID = -7370665949829689498L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String condition = null;
        String sex = req.getParameter("sex");
        String age = req.getParameter("age");
        String high = req.getParameter("high");
        String low = req.getParameter("low");
        System.out.println("sex="+sex+",age="+age+",high="+high+",low="+low);
        Integer status = test.getOnePerson(Integer.parseInt(sex), Integer.parseInt(age), Integer.parseInt(high), Integer.parseInt(low));
        if (status==1){
            condition="healthy";
        }else{
            condition="unhealthy";
        }
        req.setAttribute("age",age);
        req.setAttribute("sex",sex);
        req.setAttribute("low",low);
        req.setAttribute("high",high);
        req.setAttribute("condition",condition);
        req.getRequestDispatcher("/message.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
