package dao;

import entity.Man;
import entity.Person;
import entity.Woman;

import java.util.ArrayList;
import java.util.List;

/**
 * @author：时尚
 * @日期： 12 2020/12/4 14:46
 */
public class PatientImpl implements Patient {

    @Override
    public Person getStandardBySexAndAge(Integer sex, Integer age) {

        List<Person> people = manOrWoman(sex);

        Person person1 = new Person();

        people.forEach(person -> {
            if (Math.abs(person.getAge()-age)<=2){
                person1.setAge(age);
                person1.setHighPower(person.getHighPower());
                person1.setLowPower(person.getLowPower());
                person1.setSex(sex);
            }
        });
        return person1;
    }

    public List<Person> manOrWoman(Integer sex){
        List<Person> list = new ArrayList<>();

        if (sex==1){
            list.add(new Man(sex,115,73,18));
            list.add(new Man(sex,115,73,23));
            list.add(new Man(sex,115,75,28));
            list.add(new Man(sex,117,76,33));
            list.add(new Man(sex,120,80,38));
            list.add(new Man(sex,124,81,43));
            list.add(new Man(sex,128,82,48));
            list.add(new Man(sex,134,84,53));
            list.add(new Man(sex,137,84,58));
            list.add(new Man(sex,148,88,63));
        }
        if (sex==0){
            list.add(new Woman(sex,110,70,18));
            list.add(new Woman(sex,110,71,23));
            list.add(new Woman(sex,112,73,28));
            list.add(new Woman(sex,114,74,33));
            list.add(new Woman(sex,116,77,38));
            list.add(new Woman(sex,122,78,43));
            list.add(new Woman(sex,128,79,48));
            list.add(new Woman(sex,134,80,53));
            list.add(new Woman(sex,139,82,58));
            list.add(new Woman(sex,145,83,63));
        }
        return list;
    }
}
