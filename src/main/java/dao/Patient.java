package dao;

import entity.Person;

import java.util.List;

/**
 * @author：时尚
 * @日期： 12 2020/12/4 14:43
 */
public interface Patient {
    public Person getStandardBySexAndAge(Integer sex,Integer age);
}
