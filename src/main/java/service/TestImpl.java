package service;

import dao.Patient;
import dao.PatientImpl;
import entity.Person;

/**
 * @author：时尚
 * @日期： 12 2020/12/4 16:33
 */
public class TestImpl implements Test {
    private Patient patient = new PatientImpl();
    @Override
    public Integer getOnePerson(Integer sex, Integer age,Integer highPower,Integer lowPower) {
        Integer status = 0;
        Person se = patient.getStandardBySexAndAge(sex, age);
        Integer seHighPower = se.getHighPower();
        Integer seLowPower = se.getLowPower();
        if (seHighPower>=highPower&&seLowPower<=lowPower){
            status = 1;
        }
        return status;
    }
}
