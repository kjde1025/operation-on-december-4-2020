package entity;

/**
 * @author：时尚
 * @日期： 12 2020/12/4 14:39
 */
public class Person {
    private Integer sex;
    private Integer highPower;
    private Integer lowPower;
    private Integer age;

    public Person() {
    }

    public Person(Integer sex, Integer highPower, Integer lowPower, Integer age) {
        this.sex = sex;
        this.highPower = highPower;
        this.lowPower = lowPower;
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getHighPower() {
        return highPower;
    }

    public void setHighPower(Integer highPower) {
        this.highPower = highPower;
    }

    public Integer getLowPower() {
        return lowPower;
    }

    public void setLowPower(Integer lowPower) {
        this.lowPower = lowPower;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "sex=" + sex +
                ", highPower=" + highPower +
                ", lowPower=" + lowPower +
                ", age=" + age +
                '}';
    }
}
